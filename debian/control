Source: o2
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
Section: libs
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-exec,
 cmake,
 doxygen,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/multimedia-team/o2
Vcs-Git: https://salsa.debian.org/multimedia-team/o2.git
Homepage: https://github.com/rbdannenberg/o2/

Package: libo2-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends:
 libo2 (= ${binary:Version}),
 ${misc:Depends},
Description: next generation communication protocol for music systems - development files
 O2 is a communication protocol for interactive music and media applications.
 It is inspired by Open Sound Control (OSC) and uses similar means to form
 addresses, specify types, and encode messages.
 .
 In addition to providing message delivery, O2 offers a discovery mechanism
 where processes automatically discover and connect to other processes.
 Furthermore, O2 implements a clock synchronization protocol.
 .
 O2 is based on IP (Internet Protocol), but there are some mechanisms that allow
 an O2 process to serve as a bridge to other networks such as Bluetooth.
 .
 This package contains headers, documentation and static libraries.

Package: libo2
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 libo2-dev,
Pre-Depends:
 ${misc:Pre-Depends},
Description: next generation communication protocol for music systems
 O2 is a communication protocol for interactive music and media applications.
 It is inspired by Open Sound Control (OSC) and uses similar means to form
 addresses, specify types, and encode messages.
 .
 In addition to providing message delivery, O2 offers a discovery mechanism
 where processes automatically discover and connect to other processes.
 Furthermore, O2 implements a clock synchronization protocol.
 .
 O2 is based on IP (Internet Protocol), but there are some mechanisms that allow
 an O2 process to serve as a bridge to other networks such as Bluetooth.
